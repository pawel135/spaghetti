import java.util.Stack;



class LLVMGenerator{

    static String header_text = "";
    static String main_text = "";
    static String buffer = "";
    static String functionName = "main";
    static int reg = 1;
    static int reg_global = 1;
    static int main_reg = 1;
    static int br = 0;

    static Stack<Integer> brstack = new Stack<Integer>();

    static void icmp_i32(String op, String val1, String val2){
        buffer += "%"+reg+" = icmp "+op+" i32 "+val2+", "+val1+"\n";
        reg++;
    }

    static void icmp_double(String op, String val1, String val2){
        buffer += "%"+reg+" = fcmp "+op+" double "+val2+", "+val1+"\n";
        reg++;
    }

    static void ifstart(){
        br++;
        buffer += "br i1 %"+(reg-1)+", label %true"+br+", label %false"+br+"\n";
        buffer += "true"+br+":\n";
        brstack.push(br);
    }

    static void ifend(){
        int b = brstack.pop();
        buffer += "br label %false"+b+"\n";
        buffer += "false"+b+":\n";
    }

    static void repeatstart(String repetitions){
        declare_i32(Integer.toString(reg));
        int counter = reg;
        reg++;
        assign_i32(Integer.toString(counter), "0");
        br++;
        buffer += "br label %cond"+br+"\n";
        buffer += "cond"+br+":\n";

        load_i32("%" + counter);
        add_i32("%"+(reg-1), "1");
        assign_i32(Integer.toString(counter), "%"+(reg-1));

        buffer += "%"+reg+" = icmp slt i32 %"+(reg-2)+", "+repetitions+"\n";
        reg++;

        buffer += "br i1 %"+(reg-1)+", label %true"+br+", label %false"+br+"\n";
        buffer += "true"+br+":\n";
        brstack.push(br);
    }

    static void repeatend(){
        int b = brstack.pop();
        buffer += "br label %cond"+b+"\n";
        buffer += "false"+b+":\n";
    }


    static void functionstart(String id)
    {
        functionName = id;
        main_text += buffer;
        main_reg = reg;
        reg = 1;
        buffer = "";
    }

    static void functionend( String id, String type){
        header_text += "define "+type+" @"+functionName+"() #0 {\n";
        header_text += buffer;
        header_text += "ret "+type+" "+id+"\n";
        header_text += "}\n";
        buffer = "";
        reg = main_reg;
        functionName = "main";
    }

    static void call(String id, String type){
	    buffer += "%"+reg+" = call "+type+" @"+id+"()\n";
        reg++;
    }

    static void declare_i32_glob(String id){
        header_text += "@"+id+" = global i32 0\n";
    }

    static void declare_double_glob(String id){
        header_text += "@"+id+" = global double 0.0\n";
    }

    static void assign_i32_glob(String id, String value){
        buffer += "store i32 "+value+", i32* @"+id+"\n";
    }

    static void assign_double_glob(String id, String value){
        buffer += "store double "+value+", double* @"+id+"\n";
    }

    static void declare_i32(String id){
        buffer += "%"+id+" = alloca i32\n";
    }

    static void declare_double(String id){
        buffer += "%"+id+" = alloca double\n";
    }

    static void declare_string(String id){
        buffer += "%"+id+" = alloca i8*, align 8\n";
    }

    static void assign_i32(String id, String value){
        buffer += "store i32 "+value+", i32* %"+id+"\n";
    }

    static void assign_double(String id, String value){
        buffer += "store double "+value+", double* %"+id+"\n";
    }

    static void assign_string(String id, String value){

        //load pointer to string conteing value to assign
        buffer += "%"+reg+" = load i8** "+value+", align 8\n";
        reg++;
        //counting length and allocating memory for result string
        buffer += "%"+reg+" = call i64 @strlen(i8* %"+(reg-1)+") #5\n";
        reg++;
        buffer += "%"+reg+" = call noalias i8* @calloc(i64 %"+(reg-1)+", i64 1) #1\n";
        buffer += "store i8* %"+reg+", i8** %"+id+", align 8\n";
        reg++;
        //loding pointer to result
        buffer += "%"+reg+" = load i8** %"+id+", align 8\n";
        reg++;
        //coping string to id
        buffer += "%"+reg+" = call i8* @strcpy(i8* %"+(reg-1)+", i8* %"+(reg-4)+") #1\n";
        reg++;
    }

    static void assign_arr_int(String id, String value){
        String[] part = value.split(";");
        int byteLength = Integer.parseInt(part[2]) * 4;

        header_text += "@"+functionName+"." + id + " = private unnamed_addr constant " + part[0] + " " + part[1] + ", align 16\n";
        buffer += "%" + id + " =  alloca " + part[0] + ", align 16\n";
        buffer += "%"+reg+" = bitcast "+part[0]+"* %"+id+" to i8*\n";
        buffer += "call void @llvm.memcpy.p0i8.p0i8.i64(i8* %"+reg+", i8* bitcast ("+part[0]+"* @"+functionName+"."+id+" to i8*), i64 "+byteLength+", i32 16, i1 false)\n";
        reg++;
    }

    static void allocate_string(String value) {
        header_text += "@"+functionName+"." + reg + " = private unnamed_addr constant ["+ (value.length()+1) + " x i8] c\"" + value + "\\00\", align 1\n";
        buffer += "%" + reg + " = alloca [" + (value.length()+1) + " x i8], align 1\n";
        int start_reg = reg;
        reg++;
        buffer += "%" + reg + " = bitcast [" + (value.length()+1) + " x i8]* %"+start_reg+" to i8*\n";
        buffer += "call void @llvm.memcpy.p0i8.p0i8.i64(i8* %" + reg + ", i8* getelementptr inbounds ([" + (value.length()+1) + " x i8]* @"+functionName+"."+start_reg+", i32 0, i32 0), i64 " + (value.length()+1) + ", i32 1, i1 false)\n";
        reg++;
        buffer += "%"+reg+" = alloca i8*, align 8\n"; //this reg is result
        reg++;
        buffer += "%"+reg+" = call noalias i8* @calloc(i64 "+ (value.length()+1) + ", i64 1) #1\n";
        buffer += "store i8* %"+reg+", i8** %"+(reg-1)+", align 8\n";
        reg++;
        buffer += "%"+reg+" = load i8** %"+(reg-2)+", align 8\n";
        reg++;
        buffer += "%"+reg+" = getelementptr inbounds [" + (value.length()+1) + " x i8]* %"+start_reg+", i32 0, i32 0\n";
        reg++;
        buffer += "call i8* @strcpy(i8* %"+(reg-2)+", i8* %"+(reg-1)+") #1\n";
        reg++;
    }

    static void assign_arr_double(String id, String value){
        String[] part = value.split(";");
        int byteLength = Integer.parseInt(part[2]) * 8;

        header_text += "@"+functionName+"." + id + " = private unnamed_addr constant " + part[0] + " " + part[1] + ", align 16\n";
        buffer += "%" + id + " =  alloca " + part[0] + ", align 16\n";
        buffer += "%"+reg+" = bitcast "+part[0]+"* %"+id+" to i8*\n";
        buffer += "call void @llvm.memcpy.p0i8.p0i8.i64(i8* %"+reg+", i8* bitcast ("+part[0]+"* @"+functionName+"."+id+" to i8*), i64 "+byteLength+", i32 16, i1 false)\n";
        reg++;
    }

    static void get_arr_elem_int(String id, String def, String index) {
        String[] part = def.split(";");
        buffer += "%"+reg+" = getelementptr inbounds "+part[0]+"* %"+id+", i32 0, i64 "+index+"\n";
        reg++;
        buffer += "%"+reg+" = load i32* %"+(reg-1)+"\n";
        reg++;
    }

    static void get_arr_elem_double(String id, String def, String index) {
        String[] part = def.split(";");
        buffer += "%"+reg+" = getelementptr inbounds "+part[0]+"* %"+id+", i32 0, i64 "+index+"\n";
        reg++;
        buffer += "%"+reg+" = load double* %"+(reg-1)+"\n";
        reg++;
    }

    static void get_arr_elem_pointer(String id, String def, String index) {
        String[] part = def.split(";");
        buffer += "%"+reg+" = getelementptr inbounds "+part[0]+"* %"+id+", i32 0, i64 "+index+"\n";
        reg++;
    }

    static void load_i32(String id){
        buffer += "%"+reg+" = load i32* "+id+"\n";
        reg++;
    }

    static void load_double(String id){
        buffer += "%"+reg+" = load double* "+id+"\n";
        reg++;
    }

    static void add_i32(String val1, String val2){
        buffer += "%"+reg+" = add i32 "+val1+", "+val2+"\n";
        reg++;
    }

    static void add_double(String val1, String val2){
        buffer += "%"+reg+" = fadd double "+val1+", "+val2+"\n";
        reg++;
    }

    static int add_string(String val1, String val2) {
        int start_reg = reg;

        //allocing pointer to memory where result will be stored
        buffer += "%"+reg+" = alloca i8*, align 8\n";
        reg++;
        //counting lenghth of first string
        buffer += "%"+reg+" = load i8** "+val2+", align 8\n";
        reg++;
        buffer += "%"+reg+" = call i64 @strlen(i8* %"+(reg-1)+") #5\n";
        reg++;
        //counting length of second string
        buffer += "%"+reg+" = load i8** "+val1+", align 8\n";
        reg++;
        buffer += "%"+reg+" = call i64 @strlen(i8* %"+(reg-1)+") #5\n";
        reg++;
        //adding lenths
        buffer += "%"+reg+" = add i64 %"+(reg-3)+", %"+(reg-1)+"\n";
        reg++;
        //allocing memory for result string
        buffer += "%"+reg+" = call noalias i8* @calloc(i64 %"+(reg-1)+", i64 1) #1\n";
        buffer += "store i8* %"+reg+", i8** %"+start_reg+", align 8\n";
        reg++;
        //loding pointer to result
        buffer += "%"+reg+" = load i8** %"+start_reg+", align 8\n";
        reg++;
        //adding strings
        buffer += " %"+reg+" = call i8* @strcat(i8* %"+(reg-1)+", i8* %"+(reg-7)+") #1\n";
        reg++;
        buffer += " %"+reg+" = call i8* @strcat(i8* %"+(reg-2)+", i8* %"+(reg-6)+") #1\n";
        reg++;

        return start_reg;
    }

    static void sub_i32(String val1, String val2){
        buffer += "%"+reg+" = sub i32 "+val1+", "+val2+"\n";
        reg++;
    }

    static void sub_double(String val1, String val2){
        buffer += "%"+reg+" = fsub double "+val1+", "+val2+"\n";
        reg++;
    }

    static void mult_i32(String val1, String val2){
        buffer += "%"+reg+" = mul i32 "+val1+", "+val2+"\n";
        reg++;
    }

    static void mult_double(String val1, String val2){
        buffer += "%"+reg+" = fmul double "+val1+", "+val2+"\n";
        reg++;
    }

    static void div_i32(String val1, String val2){
        buffer += "%"+reg+" = udiv i32 "+val2+", "+val1+"\n";
        reg++;
    }

    static void div_double(String val1, String val2){
        buffer += "%"+reg+" = fdiv double "+val2+", "+val1+"\n";
        reg++;
    }

    static void length_string(String id) {
        buffer += "%"+reg+" = load i8** %"+id+", align 8\n";
        reg++;
        buffer += "%"+reg+" = call i64 @strlen(i8* %"+(reg-1)+") #5\n";
        reg++;
        buffer += "%"+reg+" = trunc i64 %"+(reg-1)+" to i32\n";
        reg++;
    }

    static void sitofp(String id){
        buffer += "%"+reg+" = sitofp i32 "+id+" to double\n";
        reg++;
    }

    static void fptosi(String id){
        buffer += "%"+reg+" = fptosi double "+id+" to i32\n";
        reg++;
    }

    static void printf_i32(String id){
        buffer += "%"+reg+" = load i32* "+id+"\n";
        reg++;
        buffer += "%"+reg+" = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* @strpi, i32 0, i32 0), i32 %"+(reg-1)+")\n";
        reg++;
    }

    static void printf_double(String id){
        buffer += "%"+reg+" = load double* "+id+"\n";
        reg++;
        buffer += "%"+reg+" = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* @strpd, i32 0, i32 0), double %"+(reg-1)+")\n";
        reg++;
    }

    static void printf_string_id(String id){
        buffer += "%"+reg+" = load i8** %"+id+"\n";
        reg++;
        buffer += "%"+reg+" = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* @strp, i32 0, i32 0), i8* %"+(reg-1)+")\n";
        reg++;
    }
    
    static void printf_string_raw(String text){
	    header_text	+= "@.str"+reg_global+" = private unnamed_addr constant ["+(text.length()+2)+" x i8] c\""+text+"\\0A\\00\", align 1\n"; // \\0A for newline
		reg_global++;
		buffer += "%"+ reg + "= call i32 (i8*, ...)* @printf(i8* getelementptr inbounds (["+(text.length()+2)+" x i8]* @.str" +(reg_global-1) + ", i32 0, i32 0))\n";
		reg++;
    }

    static void scanf_i32(String id){
        buffer += "%"+reg+" = call i32 (i8*, ...)* @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8]* @strsi, i32 0, i32 0), i32* %"+id+")\n";
        reg++;
    }

    static void scanf_double(String id){
        buffer += "%"+reg+" = call i32 (i8*, ...)* @__isoc99_scanf(i8* getelementptr inbounds ([4 x i8]* @strsd, i32 0, i32 0), double* %"+id+")\n";
        reg++;
    }

    static void scanf_string(String id){

        //allocate memory for buffer
        buffer += "%"+reg+" = alloca [64 x i8], align 16\n";
        reg++;
        //get pointer
        buffer += "%"+reg+" = getelementptr inbounds [64 x i8]* %"+(reg-1)+", i32 0, i32 0\n";
        reg++;
        //scanf to buffer
        buffer += "%"+reg+" = call i32 (i8*, ...)* @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8]* @strs, i32 0, i32 0), i8* %"+(reg-1)+")\n";
        reg++;
        //get length of actuall string
        buffer += "%"+reg+" = call i64 @strlen(i8* %"+(reg-2)+") #5\n";
        reg++;
        //allocate memory for result
        buffer += "%"+reg+" = call noalias i8* @calloc(i64 %"+(reg-1)+", i64 1) #1\n";
        buffer += "store i8* %"+reg+", i8** %"+id+", align 8\n";
        reg++;
        //load pointer to id
        buffer += "%"+reg+" = load i8** %"+id+", align 8\n";
        reg++;
        //copy
        buffer += "%"+reg+" = call i8* @strcpy(i8* %"+(reg-1)+", i8* %"+(reg-5)+") #1\n";
        reg++;
    }

    static void close_main(){
        main_text += buffer;
    }

    static String generate(){
        String text = "";
        text += "@strs = private unnamed_addr constant [3 x i8] c\"%s\\00\", align 1\n";
        text += "@strp = private unnamed_addr constant [4 x i8] c\"%s\\0A\\00\", align 1\n";
        text += "@strpi = constant [4 x i8] c\"%d\\0A\\00\"\n";
        text += "@strpd = constant [4 x i8] c\"%f\\0A\\00\"\n";
        text += "@strsi = constant [3 x i8] c\"%d\\00\"\n";
        text += "@strsd = private unnamed_addr constant [4 x i8] c\"%lf\00\", align 1\n";
        text += header_text;
        text += "define i32 @main() nounwind{\n";
        text += main_text;
        text += "ret i32 0 }\n";
        text += "declare void @llvm.memset.p0i8.i64(i8* nocapture, i8, i64, i32, i1) #1\n";
        text += "declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #1\n";
        text += "declare noalias i8* @calloc(i64, i64) #2\n";
        text += "declare i8* @strcat(i8*, i8*) #2\n";
        text += "declare i8* @strcpy(i8*, i8*) #2\n";
        text += "declare i64 @strlen(i8*) #3\n";
        text += "declare i32 @printf(i8*, ...)\n";
        text += "declare i32 @__isoc99_scanf(i8*, ...)\n";
        text += "attributes #0 = { nounwind uwtable \"less-precise-fpmad\"=\"false\" \"no-frame-pointer-elim\"=\"true\" \"no-frame-pointer-elim-non-leaf\" \"no-infs-fp-math\"=\"false\" \"no-nans-fp-math\"=\"false\" \"stack-protector-buffer-size\"=\"8\" \"unsafe-fp-math\"=\"false\" \"use-soft-float\"=\"false\" }\n";
        text += "attributes #1 = { nounwind }";
        text += "attributes #2 = { nounwind \"less-precise-fpmad\"=\"false\" \"no-frame-pointer-elim\"=\"true\" \"no-frame-pointer-elim-non-leaf\" \"no-infs-fp-math\"=\"false\" \"no-nans-fp-math\"=\"false\" \"stack-protector-buffer-size\"=\"8\" \"unsafe-fp-math\"=\"false\" \"use-soft-float\"=\"false\" }\n";
        text += "attributes #3 = { nounwind readonly \"less-precise-fpmad\"=\"false\" \"no-frame-pointer-elim\"=\"true\" \"no-frame-pointer-elim-non-leaf\" \"no-infs-fp-math\"=\"false\" \"no-nans-fp-math\"=\"false\" \"stack-protector-buffer-size\"=\"8\" \"unsafe-fp-math\"=\"false\" \"use-soft-float\"=\"false\" }\n";
        text += "attributes #5 = { nounwind readonly }\n";

        return text;
    }

}
