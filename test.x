/* autor: Paweł Szczepanowski 
	nr albumu: 252506 
	14.06.2017r. 
*/

globale x=6; // semicolon is optional, but allows for multiple statements per line
globale y=7




mostra "Program obliczajacy wynik rownania y * x^2 w sposob iteracyjny za pomoca jedynie dodawanie oraz operacji warunkowych i petli"

text="Podaj liczbe x:"
mostra text
prende int first
x = first

text="Podaj liczbe y:"
mostra text
prende int second
y = second

funzione y_times_2_to_x() // function that returns y*2^x
	mostra "----Start funkcji----"
    result = y
	above_val = 100
	str_above_hundred = "Liczba jest wieksza od " + "100" + ", jej wartosc to: "
	fai x volte
	    result = result + result
		se result > above_val
            mostra str_above_hundred
			mostra result
		finito
	finito
    ritorna result




/* a multiline comment
	/* comment inside a comment 
	*/
*/


result = y_times_2_to_x()
mostra "Wynik dzialania funkcji: "
mostra result


mostra "--------Kalkulator----------"

mostra "x="; mostra x
mostra "y="; mostra y

mostra "x+y*2="
r1 = x+y*2
mostra r1

mostra "x-(y+1)="
r2 = x-(y+1)
mostra r2

mostra "x*y="
r3 = x*y
mostra r3

mostra "x/y = (zrzutowane na liczbe rzeczywista)"
r4 = (reale)x/(reale)y
mostra r4

text = "Dlugosc tego tekstu:"
mostra text
a = lunghezza text
mostra a

mostra "-------Operacje na tablicy------"

arr=[0 10 20]
d = arr[2] + arr[1]
mostra d



