
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;

enum VarType{ INT, REAL, STRING, ARI, ARR }

class Value{
    public String name;
    public VarType type;
    public Value( String name, VarType type ){
        this.name = name;
        this.type = type;
    }
}

public class LLVMactions extends DemoBaseListener {

    HashMap<String, VarType> variablesCopy = new HashMap<String, VarType>();
    HashMap<String, VarType> variables = new HashMap<String, VarType>();
    HashMap<String, VarType> globals = new HashMap<String, VarType>();
    HashMap<String, VarType> functions = new HashMap<String, VarType>();
    HashMap<String, String> arrays = new HashMap<String, String>();
    Stack<Value> stack = new Stack<Value>();


    @Override
    public void exitIf(DemoParser.IfContext ctx) {
        LLVMGenerator.ifend();
    }

    @Override
    public void exitEqual(DemoParser.EqualContext ctx) {
        Value v1 = stack.pop();
        Value v2 = stack.pop();

        if(v1.type != v2.type) {
            error(ctx.getStart().getLine(), "Condition types must match");
        }

        if( v1.type == VarType.INT ) {
            LLVMGenerator.icmp_i32( "eq", v1.name, v2.name);
            LLVMGenerator.ifstart();
        }
        else if( v1.type == VarType.REAL ) {
            LLVMGenerator.icmp_double( "eq", v1.name, v2.name);
            LLVMGenerator.ifstart();
        }
        else {
            error(ctx.getStart().getLine(), "Conditions can use INT or REAL only.");
        }
    }

    @Override
    public void exitGreater(DemoParser.GreaterContext ctx) {
        Value v1 = stack.pop();
        Value v2 = stack.pop();

        if(v1.type != v2.type) {
            error(ctx.getStart().getLine(), "Condition types must match");
        }

        if( v1.type == VarType.INT ) {
            LLVMGenerator.icmp_i32( "ugt", v1.name, v2.name);
            LLVMGenerator.ifstart();
        }
        else if( v1.type == VarType.REAL ) {
            LLVMGenerator.icmp_double( "ugt", v1.name, v2.name);
            LLVMGenerator.ifstart();
        }
        else {
            error(ctx.getStart().getLine(), "Conditions can use INT or REAL only.");
        }
    }

    @Override
    public void exitSmaller(DemoParser.SmallerContext ctx) {
        Value v1 = stack.pop();
        Value v2 = stack.pop();

        if(v1.type != v2.type) {
            error(ctx.getStart().getLine(), "Condition types must match");
        }

        if( v1.type == VarType.INT ) {
            LLVMGenerator.icmp_i32( "ult", v1.name, v2.name);
            LLVMGenerator.ifstart();
        }
        else if( v1.type == VarType.REAL ) {
            LLVMGenerator.icmp_double( "ult", v1.name, v2.name);
            LLVMGenerator.ifstart();
        }
        else {
            error(ctx.getStart().getLine(), "Conditions can use INT or REAL only.");
        }
    }


    @Override
    public void exitStart(DemoParser.StartContext ctx) {
        Value v = stack.pop();

        if( v.type == VarType.INT ) {
            LLVMGenerator.repeatstart(v.name);
        } else {
            error(ctx.getStart().getLine(), "Number of repetitions can be INT only");
        }
    }

    @Override
    public void exitLoop(DemoParser.LoopContext ctx) {
        LLVMGenerator.repeatend();
    }

    @Override
    public void exitFname(DemoParser.FnameContext ctx) {
/*        String ID = ctx.ID().getText();
        variablesCopy = new HashMap<String, VarType>(variables);
        variables.clear();
        LLVMGenerator.functionstart(ID);
  */     
    }
    @Override
    public void enterFunctionDef(DemoParser.FunctionDefContext ctx) {
        variablesCopy = new HashMap<String, VarType>(variables);
        variables.clear();
        String ID = ctx.fname().ID().getText();
        LLVMGenerator.functionstart(ID);
    }


    @Override
    public void exitFunctionDef(DemoParser.FunctionDefContext ctx) {

        Value v = stack.pop();
        String type = "";
        if( v.type == VarType.INT ) {
            type = "i32";
        }
        else if( v.type == VarType.REAL ) {
            type = "double";
        }
        else {
            error(ctx.getStart().getLine(), "Functions can return INT or REAL only.");
        }

        functions.put(LLVMGenerator.functionName, v.type);
        LLVMGenerator.functionend( v.name, type);

        variables = new HashMap<String, VarType>(variablesCopy);
        variablesCopy.clear();
    }

    @Override
    public void exitAssignGlobal(DemoParser.AssignGlobalContext ctx) {
        String ID = ctx.ID().getText();
        Value v = stack.pop();

        if( globals.containsKey(ID) ) {
            error(ctx.getStart().getLine(), "Global values cannot be declared twice.");
        }
        if( variables.containsKey(ID) ) {
            error(ctx.getStart().getLine(), "Name allready used for local variable");
        }
        if( functions.containsKey(ID) ) {
            error(ctx.getStart().getLine(), "Name allready used for local function");
        }

        if( v.type == VarType.INT ) {
            globals.put(ID, VarType.INT);
            LLVMGenerator.declare_i32_glob(ID);
            LLVMGenerator.assign_i32_glob(ID, v.name);
        }
        else if( v.type == VarType.REAL ) {
            globals.put(ID, VarType.REAL);
            LLVMGenerator.declare_double_glob(ID);
            LLVMGenerator.assign_double_glob(ID, v.name);
        }
        else {
            error(ctx.getStart().getLine(), "Globals can be INT or REAL only.");
        }
    }

    @Override
    public void exitAssign(DemoParser.AssignContext ctx) {
        String ID = ctx.ID().getText();
        Value v = stack.pop();
        if( globals.containsKey(ID) ) {
            if( v.type == VarType.INT ) {
                LLVMGenerator.assign_i32_glob(ID, v.name);
            }
            else if( v.type == VarType.REAL ) {
                LLVMGenerator.assign_double_glob(ID, v.name);
            }
            else {
                error(ctx.getStart().getLine(), "Globals can be INT or REAL only.");
            }
        } else {
            if( ! variables.containsKey(ID) ) {
                if( v.type == VarType.INT ) {
                    variables.put(ID, VarType.INT);
                    LLVMGenerator.declare_i32(ID);
                }
                else if( v.type == VarType.REAL ) {
                    variables.put(ID, VarType.REAL);
                    LLVMGenerator.declare_double(ID);
                }
                else if( v.type == VarType.ARI ) {
                    variables.put(ID, VarType.ARI);
                    arrays.put(ID, v.name);
                    LLVMGenerator.assign_arr_int(ID, v.name);
                    return;
                }
                else if( v.type == VarType.ARR ) {
                    variables.put(ID, VarType.ARR);
                    arrays.put(ID, v.name);
                    LLVMGenerator.assign_arr_double(ID, v.name);
                    return;
                }
                else if( v.type == VarType.STRING ) {
                    variables.put(ID, VarType.STRING);
                    LLVMGenerator.declare_string(ID);
                }
            }
            if( v.type == VarType.INT ) {
                LLVMGenerator.assign_i32(ID, v.name);
            }
            else if(v.type == VarType.REAL) {
                LLVMGenerator.assign_double(ID, v.name);
            }
            else if(v.type == VarType.STRING) {
                LLVMGenerator.assign_string(ID, v.name);
            }
            else {
                error(ctx.getStart().getLine(), "Arrays cannot be reasigned.");
            }
        }
    }

    @Override
    public void exitArrayAssign(DemoParser.ArrayAssignContext ctx) {
        String ID = ctx.ID().getText();
        if(	arrays.containsKey(ID) ) {
            Value v = stack.pop();
            if( variables.get(ID) == VarType.ARI && v.type != VarType.INT) {
                error(ctx.getStart().getLine(), "Cannot assign wrong type to array");
            }
            LLVMGenerator.get_arr_elem_pointer( ID, arrays.get(ID), ctx.INT().getText() );
            if( variables.get(ID) == VarType.ARI ){
                LLVMGenerator.assign_i32(Integer.toString(LLVMGenerator.reg-1), v.name);
            }
            else if( variables.get(ID) == VarType.ARR ) {
                LLVMGenerator.assign_double(Integer.toString(LLVMGenerator.reg-1), v.name);
            }
            else {
                error(ctx.getStart().getLine(), "Cannot assign array or string to array");
            }
        } else {
            error(ctx.getStart().getLine(), ID + " is not an array or does not exist.");
        }
    }

	@Override
	public void exitFunctionCall(DemoParser.FunctionCallContext ctx){
		if( ctx.fname().ID() != null ){
            String ID = ctx.fname().ID().getText();
            if( functions.containsKey(ID) ) {
                if( functions.get(ID) == VarType.INT ) {
                    LLVMGenerator.call( ID, "i32" );
                    stack.push( new Value( "%"+(LLVMGenerator.reg-1), VarType.INT ) );
                } else if(functions.get(ID) == VarType.REAL) {
                    LLVMGenerator.call( ID, "double" );
                    stack.push( new Value( "%"+(LLVMGenerator.reg-1), VarType.REAL ) );
                } else {
                    error(ctx.getStart().getLine(), "Functions can return only INT or REAL");
                }
				/*
				String text = ctx.fname().ID().getText();
	            String value = text.substring(1, text.length()-1);
		        LLVMGenerator.allocate_string(value);
			    stack.push( new Value( "%"+(LLVMGenerator.reg-5), VarType.STRING) );
			    */
            } else {
                error(ctx.getStart().getLine(), "Unknown function: " + ID);
            }
        }
	
	}


    @Override
    public void exitValue(DemoParser.ValueContext ctx) {

        if( ctx.ID() != null ){
            String ID = ctx.ID().getText();
            if( globals.containsKey(ID) ) {
                if( globals.get(ID) == VarType.INT ) {
                    LLVMGenerator.load_i32( "@"+ID );
                    stack.push( new Value( "%"+(LLVMGenerator.reg-1), VarType.INT ) );
                } else if(globals.get(ID) == VarType.REAL) {
                    LLVMGenerator.load_double( "@"+ID );
                    stack.push( new Value( "%"+(LLVMGenerator.reg-1), VarType.REAL ) );
                } else {
                    error(ctx.getStart().getLine(), "Globals can be type INT or REAL only.");
                }
            }
            else if( variables.containsKey(ID) ) {
                if( variables.get(ID) == VarType.INT ) {
                    LLVMGenerator.load_i32( "%"+ID );
                    stack.push( new Value( "%"+(LLVMGenerator.reg-1), VarType.INT ) );
                } else if(variables.get(ID) == VarType.REAL) {
                    LLVMGenerator.load_double( "%"+ID );
                    stack.push( new Value( "%"+(LLVMGenerator.reg-1), VarType.REAL ) );
                } else if(variables.get(ID) == VarType.STRING) {
                    stack.push( new Value( "%"+ID, VarType.STRING ) );
                } else {
                    error(ctx.getStart().getLine(), "Arrays cannot be copied.");
                }
            }

            else {
                error(ctx.getStart().getLine(), "Unknown variable: " + ID);
            }
        }
        if( ctx.INT() != null ){
            stack.push( new Value( ctx.INT().getText(), VarType.INT) );
        }
        if( ctx.REAL() != null ){
            stack.push( new Value( ctx.REAL().getText(), VarType.REAL) );
        }
        if( ctx.STRING() != null) {
            String text = ctx.STRING().getText();
            String value = text.substring(1, text.length()-1);
            LLVMGenerator.allocate_string(value);
            stack.push( new Value( "%"+(LLVMGenerator.reg-5), VarType.STRING) );
        }

    }

    @Override
    public void exitArrayRead(DemoParser.ArrayReadContext ctx) {
        String ID = ctx.ID().getText();
        if( arrays.containsKey( ID ) ) {
            if(variables.get(ID) == VarType.ARI) {
                LLVMGenerator.get_arr_elem_int( ID, arrays.get(ID), ctx.INT().getText() );
                stack.push( new Value( "%"+(LLVMGenerator.reg-1), VarType.INT ) );
            } else if(variables.get(ID) == VarType.ARR) {
                LLVMGenerator.get_arr_elem_double( ID, arrays.get(ID), ctx.INT().getText() );
                stack.push( new Value( "%"+(LLVMGenerator.reg-1), VarType.REAL ) );
            } else {
                error(ctx.getStart().getLine(), "Unknown type of array: " + ID);
            }
        } else {
            error(ctx.getStart().getLine(), "Unknown array: " + ID);
        }
    }

    @Override
    public void exitAdd(DemoParser.AddContext ctx) {
        Value v1 = stack.pop();
        Value v2 = stack.pop();
        if( v1.type == v2.type ) {
            if( v1.type == VarType.INT ){
                LLVMGenerator.add_i32(v1.name, v2.name);
                stack.push( new Value("%"+(LLVMGenerator.reg-1), VarType.INT) );
            }
            else if( v1.type == VarType.REAL ){
                LLVMGenerator.add_double(v1.name, v2.name);
                stack.push( new Value("%"+(LLVMGenerator.reg-1), VarType.REAL) );
            }
            else if( v1.type == VarType.STRING ){
                int value = LLVMGenerator.add_string(v1.name, v2.name);
                stack.push( new Value("%"+value, VarType.STRING) );
            }
        } else {
            error(ctx.getStart().getLine(), "Add type mismatch");
        }
    }

    @Override
    public void exitSub(DemoParser.SubContext ctx) {
        Value v2 = stack.pop();
        Value v1 = stack.pop();
        if( v1.type == v2.type ) {
            if( v1.type == VarType.INT ){
                LLVMGenerator.sub_i32(v1.name, v2.name);
                stack.push( new Value("%"+(LLVMGenerator.reg-1), VarType.INT) );
            }
            else if( v1.type == VarType.REAL ){
                LLVMGenerator.sub_double(v1.name, v2.name);
                stack.push( new Value("%"+(LLVMGenerator.reg-1), VarType.REAL) );
            } else {
                error(ctx.getStart().getLine(), "Invalid operation");
            }
        } else {
            error(ctx.getStart().getLine(), "Sub type mismatch");
        }
    }

    @Override
    public void exitArrayi(DemoParser.ArrayiContext ctx) {
        String arrayDef = "[";
        int arrayLen = 0;

        for (TerminalNode item : ctx.INT()) {
            arrayDef += "i32 " + item.getText() + ",";
            arrayLen++;
        }
        arrayDef = arrayDef.substring(0, arrayDef.length()-1);
        arrayDef += "]";
        String finalText = "[" + arrayLen + " x i32];" + arrayDef +";" + arrayLen;
        stack.push(new Value(finalText, VarType.ARI) );
    }

    @Override
    public void exitArrayr(DemoParser.ArrayrContext ctx) {
        String arrayDef = "[";
        int arrayLen = 0;

        for (TerminalNode item : ctx.REAL()) {
            arrayDef += "double " + item.getText() + ",";
            arrayLen++;
        }
        arrayDef = arrayDef.substring(0, arrayDef.length()-1);
        arrayDef += "]";
        String finalText = "[" + arrayLen + " x double];" + arrayDef +";" + arrayLen;
        stack.push(new Value(finalText, VarType.ARR) );
    }

    @Override
    public void exitMult(DemoParser.MultContext ctx) {
        Value v1 = stack.pop();
        Value v2 = stack.pop();
        if( v1.type == v2.type ) {
            if( v1.type == VarType.INT ){
                LLVMGenerator.mult_i32(v1.name, v2.name);
                stack.push( new Value("%"+(LLVMGenerator.reg-1), VarType.INT) );
            }
            else if( v1.type == VarType.REAL ){
                LLVMGenerator.mult_double(v1.name, v2.name);
                stack.push( new Value("%"+(LLVMGenerator.reg-1), VarType.REAL) );
            } else {
                error(ctx.getStart().getLine(), "Invalid operation");
            }
        } else {
            error(ctx.getStart().getLine(), "Mult type mismatch");
        }
    }

    @Override
    public void exitDiv(DemoParser.DivContext ctx) {
        Value v1 = stack.pop();
        Value v2 = stack.pop();
        if( v1.type == v2.type ) {
            if( v1.type == VarType.INT ){
                LLVMGenerator.div_i32(v1.name, v2.name);
                stack.push( new Value("%"+(LLVMGenerator.reg-1), VarType.INT) );
            }
            else if( v1.type == VarType.REAL ){
                LLVMGenerator.div_double(v1.name, v2.name);
                stack.push( new Value("%"+(LLVMGenerator.reg-1), VarType.REAL) );
            } else {
                error(ctx.getStart().getLine(), "Invalid operation");
            }
        } else {
            error(ctx.getStart().getLine(), "Div type mismatch");
        }
    }

    @Override
    public void exitToint(DemoParser.TointContext ctx) {
        Value v = stack.pop();
        LLVMGenerator.fptosi( v.name );
        stack.push( new Value("%"+(LLVMGenerator.reg-1), VarType.INT) );
    }

    @Override
    public void exitToreal(DemoParser.TorealContext ctx) {
        Value v = stack.pop();
        LLVMGenerator.sitofp( v.name );
        stack.push( new Value("%"+(LLVMGenerator.reg-1), VarType.REAL) );
    }

    @Override
    public void exitProg(DemoParser.ProgContext ctx) {
        LLVMGenerator.close_main();
        System.out.println( LLVMGenerator.generate() );
    }
    
    @Override
    public void exitWrite(DemoParser.WriteContext ctx) {
		if( ctx.value().ID() != null ){
			String ID = ctx.value().ID().getText();
			if( globals.containsKey(ID) ) {
				if( globals.get(ID) == VarType.INT ) {
					LLVMGenerator.printf_i32( "@"+ID );
				}
				else if ( globals.get(ID) == VarType.REAL ){
					LLVMGenerator.printf_double( "@"+ID );
				}
			}
			else if( variables.containsKey(ID) ) {
				if( variables.get(ID) == VarType.INT ) {
					LLVMGenerator.printf_i32( "%"+ID );
				}
				else if ( variables.get(ID) == VarType.REAL ){
					LLVMGenerator.printf_double( "%"+ID );
				}
				else if ( variables.get(ID) == VarType.STRING ){
					LLVMGenerator.printf_string_id( ID );
				}
			} else {
				error(ctx.getStart().getLine(), "Unknown variable: "+ID);
			}
		} else if( ctx.value().INT() != null ) {
			LLVMGenerator.printf_string_raw( ctx.value().INT().getText() );
		} else if( ctx.value().REAL() != null ) {
			LLVMGenerator.printf_string_raw( ctx.value().REAL().getText() );
		} else if( ctx.value().STRING() != null ) {
			String text = ctx.value().STRING().getText();
			text = text.substring(1, text.length()-1);
			text = text.replaceAll("\n","\\0A");
			LLVMGenerator.printf_string_raw( text );
		} else {
            error(ctx.getStart().getLine(), "Cannot print the element specified");

		}
	}

    @Override
    public void exitRead(DemoParser.ReadContext ctx) {
        String ID = ctx.ID().getText();
        //System.out.println( " exit read variable " + variables.get(ID) );
        if( !variables.containsKey(ID) ) {
            error(ctx.getStart().getLine(), "Unknown variable: "+ID);
        } else {
            if( variables.get(ID) == VarType.INT ) {
                LLVMGenerator.scanf_i32(ID);
            } else if ( variables.get(ID) == VarType.REAL ) {
                LLVMGenerator.scanf_double(ID);
            }
            else if( variables.get(ID) == VarType.STRING ) {
                LLVMGenerator.scanf_string(ID);
            } else {
                error(ctx.getStart().getLine(), "Cannot read array");
            }
        }
    }

    @Override
    public void exitReadi(DemoParser.ReadiContext ctx) {
        String ID = ctx.ID().getText();
        if( !variables.containsKey(ID) ) {
            variables.put(ID, VarType.INT);
            LLVMGenerator.declare_i32(ID);
            LLVMGenerator.assign_i32(ID, "0");
            LLVMGenerator.scanf_i32(ID);
        } else {
            if(	variables.get(ID) == VarType.INT ) {
                LLVMGenerator.scanf_i32(ID);
            }
            else {
                error(ctx.getStart().getLine(), "Cannot read type int to another type");
            }
        }
    }
    @Override
    public void exitReads(DemoParser.ReadsContext ctx) {
        String ID = ctx.ID().getText();

        if( !variables.containsKey(ID) ) {
            variables.put(ID, VarType.STRING);
            String value = "";
            LLVMGenerator.allocate_string(value);
            value = "%"+(LLVMGenerator.reg-5);
            LLVMGenerator.declare_string(ID);
            LLVMGenerator.assign_string(ID, value);
            LLVMGenerator.scanf_string(ID);
        } else {
            if(	variables.get(ID) == VarType.STRING ) {
                LLVMGenerator.scanf_string(ID);
            }
            else {
                error(ctx.getStart().getLine(), "Cannot read type int to another type");
            }
        }

    }

    @Override
    public void exitReadd(DemoParser.ReaddContext ctx) {
        String ID = ctx.ID().getText();

        if( !variables.containsKey(ID) ) {
            variables.put(ID, VarType.REAL);
            LLVMGenerator.declare_double(ID);
            LLVMGenerator.assign_double(ID, "0.0");
            LLVMGenerator.scanf_double(ID);
        } else {
            if(	variables.get(ID) == VarType.REAL ) {
                LLVMGenerator.scanf_double(ID);
            }
            else {
                error(ctx.getStart().getLine(), "Cannot read type double to another type");
            }
        }
    }

    @Override
    public void exitLength(DemoParser.LengthContext ctx) {
        String ID = ctx.ID().getText();

        if( !variables.containsKey(ID) ) {
            error(ctx.getStart().getLine(), "Unknown variable: "+ID);
        } else {
            if(	variables.get(ID) == VarType.INT || variables.get(ID) == VarType.REAL ) {
                stack.push( new Value( "1", VarType.INT) );
            }
            else if( variables.get(ID) == VarType.STRING ) {
                LLVMGenerator.length_string(ID);
                stack.push( new Value( "%"+(LLVMGenerator.reg-1), VarType.INT) );
            }
            else if( variables.get(ID) == VarType.ARI || variables.get(ID) == VarType.ARI) {
                String def = arrays.get(ID);
                String[] part = def.split(";");
                stack.push( new Value( part[2], VarType.INT) );
            }
        }
    }

    void error(int line, String msg){
        System.err.println("Error, line "+line+", "+msg);
        System.exit(1);
    }

}
