grammar Demo;

tokens { KEYWORD }

/*channels {
	WHITESPACE_CHANNEL,
	COMMENTS_CHANNEL
}
*/
	
prog: ( ( statement | functionDef)? (NEWLINE|SEMICOLON) )*
    ;

functionDef: FUNCTION fname params block RETURN expression
	;

statement: WRITE value				#write
	| <assoc=right> ID '=' expression				#assign
	| <assoc=right> GLOBAL ID '=' expression		#assignGlobal
	| <assoc=right> ID '[' INT '] =' expression	#arrayAssign
	| READ ID			   		#read
	| READ 'int' ID		   		#readi
	| READ 'double' ID 		   	#readd
	| READ 'string' ID			#reads
	| DO expression start block END  #loop
	| IF cond block END 		#if
	;

expression: 
     '(' expression ')'					#par
    | TOINT expression					#toint
    | TOREAL expression					#toreal
	| LENGTH ID							#length
	| '[' INT+ ']'						#arrayi
	| '[' REAL+ ']'						#arrayr
    | expression MULT expression		#mult
    | expression DIV expression			#div
	| expression ADD expression			#add
    | expression SUB expression			#sub
	| functionCall						#funcall
	| ID '[' INT ']'					#arrayRead
	| value								#id
	;


cond: expression '==' expression			#equal
	| expression '>' expression			#greater
	| expression '<' expression			#smaller
	;

functionCall: 
	| fname '()'
	;

	

fname: ID
	;

fnamecall: ID
	;

/* TODO: */

params:	'(' args+=value+ ')'	#someparams
	| '()'						#noparams
	;

block: ( statement? NEWLINE )*
	;

value: ID
	| INT
	| REAL
	| STRING
    ;

start: TIMES
	;

IF: 'se'
	;

DO: 'fai'
	;

TIMES: 'volte'
	;

END: 'finito'
	;

FUNCTION: 'funzione'
	;

RETURN:	'ritorna'
	;

TOINT: '(intero)'
    ;

TOREAL: '(reale)'
    ;

LENGTH: 'lunghezza'
	;

WRITE: 'mostra'
    ;

READ: 'prende'
    ;

GLOBAL: 'globale'
    ;

STRING: '"' ( ~('\\'|'"') )* '"' /* {System.err.println("String " );} */
    ;


ID: LETTER WORD_CHAR*
    ;

REAL: DIGIT+ '.' DIGIT*
    ;

INT: DIGIT+
    ;

ADD: '+'
    ;

SUB: '-'
    ;

DIV: '/'
    ;

MULT: '*'
    ;

	
NEWLINE: '\r'? '\n'
    ;

SEMICOLON: ';'
	;

DIGIT: '0'..'9'
	;

LETTER: 'a'..'z'|'A'..'Z'
	;

WORD_CHAR: LETTER|DIGIT|'_'
	;

/*
WS: (' '|'\t')+ { skip(); }
    ;
*/
WS: [ \t]+ -> channel(HIDDEN)
	;

COMMENT : '/*' (COMMENT|.)*? '*/' -> channel(HIDDEN)
	;
LINE_COMMENT  : '//' .*? '\n' -> channel(HIDDEN)
	;


